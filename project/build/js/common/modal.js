
riot.tag2('modal', '<div><yield></yield></div>', '', 'show="{visible}"', function(opts) {
    this.visible = false;

    this.on('mount', () => {
      const confirm = this.root.querySelector('.confirm');
      if (confirm) {
        confirm.addEventListener('click', (ev) => {
          if (this.opts.onConfirm) {
            this.opts.onConfirm(this.data, this);
          }

          this.hide();
          this.update();
          ev.stopPropagation();
          this.trigger('confirm');
        });
      }

      const cancel = this.root.querySelector('.cancel');
      if (cancel) {
        cancel.addEventListener('click', (ev) => {
          if (this.opts.onCancel) {
            this.opts.onCancel(this.data, this);
          }

          this.hide();
          this.update();
          ev.stopPropagation();
          this.trigger('cancel');
        });
      }
    });

    this.show = () => {
      this.visible = true;
    }

    this.hide = () => {
      this.visible = false;
    }
});