
riot.tag2('sprite-tools', '<div class="tools left" ref="left_tools"> <div class="slide-panel left {left_hovering ? \'hovering\' : \'\'}"> <div class="top"> <div class="undo-redo"> <button class="undo" onclick="{undo}"><=</button> <button class="redo" onclick="{redo}">=></button> </div> <div class="sprite-card" each="{user_sprites}"><img riot-src="{sprite.texture.baseTexture.imageUrl}"></div> </div> <div class="bottom"> <button class="single" onclick="{new_label}">Add Label</button> </div> </div> </div> <div class="tools right" ref="right_tools"> <div class="slide-panel right {right_hovering ? \'hovering\' : \'\'}"> <div class="top"> <button class="single" onclick="{foreground}">Foreground</button> <button class="single" onclick="{forward}">Bring forward</button> <button class="single" onclick="{backward}">Move backward</button> <button class="single" onclick="{background}">Background</button> </div> <div class="bottom"> <button class="single" onclick="{open_settings_modal}">Settings</button> </div> </div> </div> <modal ref="settings_modal"> <div class="title">Settings</div> <div class="content"> <div class="red bold" if="{parent.error}">Please enter valid hex color code</div> <label>Background Color: <input ref="background_input" type="text" oninput="{parent.change_background}"> </label> <div class="buttons"> <button class="cancel">Ok</button> </div> </div> </modal>', '', '', function(opts) {
    this.user_sprites = [];

    this.on('mount', () => {
      this.hovering = false;

      this.refs.left_tools.onmouseover = () => {
        this.update({ left_hovering: true });
      };

      this.refs.left_tools.onmouseout = () => {
        this.update({ left_hovering: false });
      };

      this.refs.right_tools.onmouseover = () => {
        this.update({ right_hovering: true });
      };

      this.refs.right_tools.onmouseout = () => {
        this.update({ right_hovering: false });
      };

      const get_user_sprites = () => {
        this.user_sprites = lib.editor.userSpriteManager.user_sprites;
        this.update();
        window.removeEventListener('load_user_sprites', get_user_sprites);
      };

      if (
        window.lib &&
        window.lib.editor &&
        window.lib.editor.userSpriteManager &&
        window.lib.editor.userSpriteManager.user_sprites
      ) {
        this.user_sprites = lib.editor.userSpriteManager.user_sprites;
        this.update();
      }
      else {
        window.addEventListener('load_user_sprites', get_user_sprites);
      }
    });

    this.foreground = () => {
      lib.editor.userSpriteManager.rearrange('foreground');
    };

    this.forward = () => {
      lib.editor.userSpriteManager.rearrange('forward');
    };

    this.backward = () => {
      lib.editor.userSpriteManager.rearrange('backward');
    };

    this.background = () => {
      lib.editor.userSpriteManager.rearrange('background');
    };

    this.undo = () => {
      lib.editor.userSpriteManager.undo();
    };

    this.redo = () => {
      lib.editor.userSpriteManager.redo();
    };

    this.new_label = () => {
      lib.editor.userSpriteManager.create_label();
    };

    this.open_settings_modal = () => {
      const hex = lib.editor.theme.background.toString(16);
      this.refs.settings_modal.refs.background_input.value = '#' + hex;
      this.refs.settings_modal.show();
    };

    this.change_background = (ev) => {
      this.error = false;
      const value = ev.target.value;

      if (value === '') {
        this.refs.settings_modal.refs.background_input.value = '#';
      }

      if (value.match(/^#([0-9a-f]{3}|[0-9a-f]{6})$/i)) {
        lib.editor.theme.new_background_color(value);
      }
      else {
        this.error = true;
      }
    };
});