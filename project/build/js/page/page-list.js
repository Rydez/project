
riot.tag2('page-list', '<h1>Pages</h1><br> <label>Slug: <input ref="slug" type="text" oninput="{check_for_slug}"> </label> <button onclick="{create_page}" disabled="{!slug_exists}">Create Page</button><br> <h2 if="{!pages}">Loading...</h2> <h2 if="{pages &amp;&amp; pages.length === 0}">No pages to display</h2> <div class="page-link" if="{pages.length !== 0}" each="{pages}"><a href="#pages/{id}/edit">{slug}</a> <button onclick="{delete_page}">Delete</button> </div>', '', '', function(opts) {
    this.on('mount', () => {
      lib.req.get('pages').then(pages => {
        this.update({ pages });
      });
    });

    this.create_page = (ev) => {
      const slug = this.refs.slug.value;
      lib.req.post('pages', { slug }).then(new_page => {
        route(`/pages/${new_page.id}/edit`);
      });
    };

    this.check_for_slug = () => {
      this.update({ slug_exists: this.refs.slug.value !== '' });
    };

    this.delete_page = (ev) => {
      const page_id = ev.item.id;
      lib.req.delete(`pages/${page_id}`)
        .then(() => {
          this.pages = this.pages.filter(p => p.id !== page_id);
          this.update()
        });
    };
});