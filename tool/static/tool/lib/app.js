
import { theme } from './theme.js';
import { upload } from './upload.js';
import { userSpriteManager } from './user-sprite/user-sprite-manager.js';
import { resize } from './resize.js';

class App {
  constructor() {

  }

  initialize(options) {
    this.options = options;
    [this.container] = document.getElementsByTagName('main');
    this.pixi_app = new PIXI.Application({
      width: screen.width,
      height: screen.height
    });

    if (this.pixi_app.renderer instanceof PIXI.CanvasRenderer) {
      console.log('rendering with canvas');
    }
    else {
      console.log('rendering with webgl');
    }

    this.container.appendChild(this.pixi_app.view);

    resize.initialize();
    theme.initialize();
    upload.initialize();
    userSpriteManager.initialize();
  }

  destroy() {
    this.pixi_app.destroy(true, true);
  }
}

window.lib = window.lib || {};
window.lib.editor = window.lib.editor || {};
window.lib.editor.app = window.lib.editor.app || new App();

export const app = window.lib.editor.app;

window.dispatchEvent(new Event('load_pixi_app'));
