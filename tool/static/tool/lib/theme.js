import { app } from './app.js';
import { req } from './req.js';

class Theme {
  constructor () {
    this.background = 0x111111;
    this.percentage_lighter = 20;
    this.percentage_darker = -20;
    this.shadow = {
      color: 0x000000,
      alpha: 0.2,
      blur: 6,
      distance: 20
    };

    this.text = {
      fontFamily: 'Arial',
      fontSize: 24,
      fill: 0xFFFFFF,
      align: 'center'
    }

    this.main = document.querySelector('main');
  }

  initialize() {
    req.get(`pages/${app.options.page_id}`)
      .then(page => {
        const color_number = page.background_color || this.background;
        const hex = color_number.toString(16);
        this.change_background_color(hex);
      });
  }

  drag_enter() {
    app.pixi_app.renderer.backgroundColor = this.lit_background;
  }

  drag_exit() {
    app.pixi_app.renderer.backgroundColor = this.background;
  }

  drop() {
    app.pixi_app.renderer.backgroundColor = this.background;
  }

  add_shadow(graphic) {
    const shadow = new PIXI.filters.DropShadowFilter();
    shadow.color = theme.shadow.color;
    shadow.alpha = theme.shadow.alpha;
    shadow.blur = theme.shadow.blur;
    shadow.distance = theme.shadow.distance;
    graphic.filters = [shadow];
  }

  new_background_color(hex) {
    this.change_background_color(hex);
    req.patch(`pages/${app.options.page_id}`, {
      background_color: this.background
    });
  }

  change_background_color(hex) {
    hex = hex.replace(/^#/, '')
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }

    this.background = parseInt(hex, 16);
    const shade = this.shade_color(hex, this.percentage_lighter);
    this.lit_background = parseInt(shade.replace(/^#/, ''), 16);
    app.pixi_app.renderer.backgroundColor = this.background;
    if (app.options.mode !== 'edit') {
      this.main.style.background = '#' + hex;
    }
  }

  shade_color(color, percent) {
    if (!color.includes('#')) {
      color = '#' + color;
    }

    let red = parseInt(color.substring(1, 3), 16);
    let green = parseInt(color.substring(3, 5), 16);
    let blue = parseInt(color.substring(5, 7), 16);

    red = parseInt(red * (100 + percent) / 100);
    green = parseInt(green * (100 + percent) / 100);
    blue = parseInt(blue * (100 + percent) / 100);

    red = (red < 255 ? red : 255).toString(16);
    green = (green < 255 ? green : 255).toString(16);
    blue = (blue < 255 ? blue : 255).toString(16);

    red = red.length === 1 ? '0' + red : red;
    green = green.length === 1 ? '0' + green: green;
    blue = blue.length === 1 ? '0' + blue: blue;

    return '#' + red + green + blue;
  }

  get_darker_shade() {
    const hex = '#' + this.background.toString(16);
    const shade = this.shade_color(hex, this.percentage_darker);
    return parseInt(shade.replace(/^#/, ''), 16);
  }
};

window.lib = window.lib || {}
window.lib.editor = window.lib.editor || {};
window.lib.editor.theme = window.lib.editor.theme || new Theme();

export const theme = window.lib.editor.theme;