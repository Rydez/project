import { app } from './app.js'
import { theme } from './theme.js';
import { userSpriteManager } from './user-sprite/user-sprite-manager.js'

class Upload {

  initialize() {
    if (app.options.mode !== 'edit') {
      app.pixi_app.view.ondragover = null;
      app.pixi_app.view.ondragleave = null;
      app.pixi_app.view.ondrop = null;
      return;
    }

    // Use drag count to prevent calling functions more than needed
    let drag_count = 0;

    app.pixi_app.view.ondragover = (e) => {
      e.preventDefault();
      if (drag_count === 0) {
        drag_count++;
        theme.drag_enter();
      }
    };

    app.pixi_app.view.ondragleave = (e) => {
      if (drag_count === 1) {
        drag_count--;
        theme.drag_exit();
      }
    };

    app.pixi_app.view.ondrop = (e) => {
      e.preventDefault();
      drag_count = 0;
      theme.drop();
      userSpriteManager.sprite_from_file(e);
    };
  }
};

export const upload = new Upload();