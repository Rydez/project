import { app } from './app.js'
import { userSpriteManager } from './user-sprite/user-sprite-manager.js';

class Resize {
  constructor () {

  }

  initialize() {
    if (app.options.mode !== 'edit') {
      window.onresize = this.resize_display.bind(this);
      return;
    }

    window.onresize = userSpriteManager.transform_to_center;
  }

  set_background(background) {
    this.orig_app_w = background.width;
    this.orig_app_h = background.height;
    this.resize_display();
  }

  resize_display() {
    const orig_app_w = this.orig_app_w || app.pixi_app.renderer.width;
    const orig_app_h = this.orig_app_h || app.pixi_app.renderer.height;
    const win_w = window.innerWidth;
    const win_h = window.innerHeight;
    const orig_app_ratio = orig_app_w / orig_app_h;
    const win_ratio = win_w / win_h;

    let app_w;
    let app_h;
    if (win_w >= orig_app_w && win_h >= orig_app_h) {
      app_w = orig_app_w;
      app_h = orig_app_h;
    }
    else if (win_ratio < orig_app_ratio) {
      const ratio = orig_app_h / orig_app_w;
      app_w = win_w;
      app_h = win_w * ratio;
    }
    else if (win_ratio >= orig_app_ratio) {
      const ratio = orig_app_w / orig_app_h;
      app_h = win_h;
      app_w = win_h * ratio;
    }

    app.pixi_app.renderer.resize(app_w, app_h);
    app.pixi_app.stage.scale.x = app_w / orig_app_w;
    app.pixi_app.stage.scale.y = app_h / orig_app_h;
  }
};

export const resize = new Resize();