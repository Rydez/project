class Req {
  constructor() {
    this.api = '/api/';
  }

  post(key, data, options) {
    return this.create_request('POST', key, data, options);
  }

  patch(key, data, options) {
    return this.create_request('PATCH', key, data, options);
  }

  delete(key, options) {
    return this.create_request('DELETE', key, null, options);
  }

  get(key, options) {
    return this.create_request('GET', key, null, options);
  }

  create_request(method, key, data, options) {
    const csrf_token = Cookies.get('csrftoken');
    const headers = new Headers();
    headers.set('X-CSRFToken', csrf_token);
    headers.set('content-type', 'application/json');

    options = options || {};
    options.method = method;
    if (data) {
      options.body = JSON.stringify(data);
    }
    options.headers = headers;

    const url = `${this.api}${key}/`;
    const request = new Request(url, options);
    let ok = false;
    return fetch(request)
      .then(response => {
        if (response.ok) {
          ok = true;
        }

        if (method === 'DELETE') {
          return;
        }

        return response.json();
      })
      .then(json => {
        if (ok) {
          if (method === 'DELETE') {
            return;
          }

          return json;
        }

        const keys = Object.keys(json);
        const error = new Error(`${keys[0]}: ${json[keys[0]]}`);
        console.log(error);
        throw error;
      });
  }
}

window.lib = window.lib || {}
window.lib.req = window.lib.req || new Req();

export const req = window.lib.req;