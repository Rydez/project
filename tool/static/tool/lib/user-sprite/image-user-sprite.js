import { app } from '../app.js';
import UserSprite from './user-sprite.js';

export default class ImageUserSprite extends UserSprite {
  constructor (source, options) {
    super(options);
    this.sprite = PIXI.Sprite.from(source);
    this.container.addChild(this.sprite);

    this.initialize();

    const stage_sprite = () => {
      this.fit_to_editor();

      if (this.z !== 0 && app.options.mode === 'edit') {
        this.sprite.interactive = true;
        this.sprite.buttonMode = true;
      }

      if (this.options.is_new) {
        this.save_sprite({
          image: this.sprite.texture.baseTexture.imageUrl,
          x: this.container.x,
          y: this.container.y,
          z: this.z,
          width: this.sprite.width,
          height: this.sprite.height,
          page: app.options.page_id,
          type: 'image'
        });
      }
      else {
        this.id = this.options.id;
        this.container.name = this.id.toString();
        // this.history_set = this.options.history_set;
        // this.history_index = this.history_set.length;
      }

      app.pixi_app.stage.addChild(this.container);
    };

    const checkLoaded = () => {
      if (this.sprite.texture.baseTexture.hasLoaded) {
        stage_sprite();
        this.sprite.texture.off('update', checkLoaded);
      }
    };

    if (this.sprite.texture.baseTexture.hasLoaded) {
      stage_sprite();
    }
    else {
      this.sprite.texture.on('update', checkLoaded);
    }
  }

  fit_to_editor() {
    this.container.width = this.sprite.width;
    this.container.height = this.sprite.height;

    this.orig_sprite_width = this.sprite.width
    this.orig_sprite_height = this.sprite.height

    this.sprite.anchor.set(0.5);

    const center_x = window.innerWidth / 2;
    const center_y = window.innerHeight / 2;

    this.z = this.options.z == null ? app.pixi_app.stage.children.length : this.options.z;

    this.container.x = this.options.x == null ? center_x : this.options.x;
    this.container.y = this.options.y == null ? center_y : this.options.y;

    this.sprite.x = 0;
    this.sprite.y = 0;

    if (this.options.width && this.options.height) {
      this.sprite.width = this.options.width;
      this.sprite.height = this.options.height;
    }
  }
};