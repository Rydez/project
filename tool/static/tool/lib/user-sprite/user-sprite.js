import { app } from '../app.js';
import { req } from '../req.js';
import { userSpriteManager as manager } from './user-sprite-manager.js';

export default class UserSprite {
  constructor (options) {
    this.options = options || {};
    this.container = new PIXI.Container();

    if (!this.options.is_new) {
      manager.user_sprites.push(this);
    }
  }

  initialize() {
    this.sprite.on('pointerdown', this.select.bind(this));

    // Dragging
    this.sprite.on('pointerdown', this.start_drag.bind(this));
    this.sprite.on('pointermove', this.drag.bind(this));
    this.sprite.on('pointerup', this.end_drag.bind(this));
  };

  save_sprite(data) {
    req.post('sprites', data)
      .then(sprite => {
        this.id = sprite.id;
        this.container.name = this.id.toString();
        manager.user_sprites.push(this);
      })
      .catch(err => {
        alert(err)
        this.container.destroy(true)
      });
  }

  select() {
    if (!this.outline || !this.corners) {
      this.draw_selection();
    }

    for (let user_sprite of manager.user_sprites) {
      user_sprite.unselect();
    }

    this.sprite.addChild(this.outline);
    this.container.addChild(this.corners);

    this.selected = true;
  }

  unselect() {
    if (this.outline && this.corners) {
      this.sprite.removeChild(this.outline);
      this.container.removeChild(this.corners);
    }

    this.selected = false;
  }

  draw_selection() {
    const length = 15; // length of square

    // Outline
    this.outline = new PIXI.Graphics();
    this.outline.nativeLines = true;
    this.outline.lineStyle(1, 0xFFFFFF, 0.5, 1);
    const sprite_bounds = this.sprite.getLocalBounds()
    this.outline.drawRect(0, 0, sprite_bounds.width, sprite_bounds.height);
    this.outline.pivot.x = sprite_bounds.width / 2;
    this.outline.pivot.y = sprite_bounds.height / 2;

    // Corner squares
    this.corners = new PIXI.Container();
    const corner_square = (name, x, y) => {
      const graphic = new PIXI.Graphics();
      graphic.nativeLines = true;
      graphic.lineStyle(1, 0xFFFFFF, 0.5, 1);
      graphic.beginFill(0x000000, 0.7)
      graphic.drawRect(x, y, length, length);
      const texture = app.pixi_app.renderer.generateTexture(graphic)
      this[name] = new PIXI.Sprite(texture);
      this[name].interactive = true;
      this[name].buttonMode = true;
      this[name].anchor.set(0.5);
      this[name].x = x;
      this[name].y = y;

      this.corners.addChild(this[name]);

      this[name].on('pointerdown', this.start_resize.bind(this, name));
      this[name].on('pointermove', this.resize.bind(this, name));
      this[name].on('pointerup', this.end_resize.bind(this, name));
    };

    let square_x = -this.sprite.width / 2;
    let square_y = -this.sprite.height / 2;
    corner_square('top_left', square_x, square_y);

    square_x = this.sprite.width / 2;
    corner_square('top_right', square_x, square_y);

    square_x = -this.sprite.width / 2;
    square_y = this.sprite.height / 2;
    corner_square('bottom_left', square_x, square_y);

    square_x = this.sprite.width / 2;
    corner_square('bottom_right', square_x, square_y);
  }

  redraw_selection() {
    this.unselect();

    if (this.outline && this.corners) {
      this.outline.destroy(true);
      this.corners.destroy(true);
      delete this.outline;
      delete this.corners;
    }

    this.select();
  }

  start_drag() {
    this.dragging = true;
    const pos = app.pixi_app.renderer.plugins.interaction.mouse.global;
    this.last_mouse_x = pos.x;
    this.last_mouse_y = pos.y;
  }

  drag() {
    if (this.dragging) {
      const pos = app.pixi_app.renderer.plugins.interaction.mouse.global;

      this.container.x += (pos.x - this.last_mouse_x);
      this.container.y += (pos.y - this.last_mouse_y);

      this.last_mouse_x = pos.x;
      this.last_mouse_y = pos.y;
    }
  }

  end_drag() {
    for (let user_sprite of manager.user_sprites) {
      if (user_sprite.dragging) {
        user_sprite.dragging = false;

        const data = {
          x: user_sprite.container.x,
          y: user_sprite.container.y
        };

        if (this.history_index != null) {
          const history = this.history_set[this.history_index];
          if (history) {
            data.clear_history_id = history.id;
          }
        }

        req.patch(`sprites/${user_sprite.id}`, data)
          .then(sprite => {
            manager.update_histories();
            const index = manager.sprite_id_index;
            manager.sprite_id_history = manager.sprite_id_history.slice(0, index);
            manager.sprite_id_history.push(sprite.id);
            manager.sprite_id_index = manager.sprite_id_history.length;
          });
      }
    }
  }

  start_resize(corner) {
    this[corner].resizing = true;
    const pos = app.pixi_app.renderer.plugins.interaction.mouse.global;
    this.last_mouse_x = pos.x;
    this.last_mouse_y = pos.y;
  }

  resize(corner) {
    if (this[corner].resizing) {
      const pos = app.pixi_app.renderer.plugins.interaction.mouse.global;

      const diff_x = pos.x - this.last_mouse_x;
      const diff_y = pos.y - this.last_mouse_y;

      if (corner === 'bottom_right') {
        this.top_left.x -= diff_x;
        this.top_left.y -= diff_y;
        this.top_right.x += diff_x;
        this.top_right.y -= diff_y;
        this.bottom_left.x -= diff_x;
        this.bottom_left.y += diff_y;
        this.bottom_right.x += diff_x;
        this.bottom_right.y += diff_y;

        this.sprite.scale.x += (2 * diff_x) / this.orig_sprite_width;
        this.sprite.scale.y += (2 * diff_y) / this.orig_sprite_height;
      }
      else if (corner === 'top_right') {
        this.top_left.x -= diff_x;
        this.top_left.y += diff_y;
        this.top_right.x += diff_x;
        this.top_right.y += diff_y;
        this.bottom_left.x -= diff_x;
        this.bottom_left.y -= diff_y;
        this.bottom_right.x += diff_x;
        this.bottom_right.y -= diff_y;

        this.sprite.scale.x += (2 * diff_x) / this.orig_sprite_width;
        this.sprite.scale.y -= (2 * diff_y) / this.orig_sprite_height;
      }
      else if (corner === 'top_left') {
        this.top_left.x += diff_x;
        this.top_left.y += diff_y;
        this.top_right.x -= diff_x;
        this.top_right.y += diff_y;
        this.bottom_left.x += diff_x;
        this.bottom_left.y -= diff_y;
        this.bottom_right.x -= diff_x;
        this.bottom_right.y -= diff_y;

        this.sprite.scale.x -= (2 * diff_x) / this.orig_sprite_width;
        this.sprite.scale.y -= (2 * diff_y) / this.orig_sprite_height;
      }
      else if (corner === 'bottom_left') {
        this.top_left.x += diff_x;
        this.top_left.y -= diff_y;
        this.top_right.x -= diff_x;
        this.top_right.y -= diff_y;
        this.bottom_left.x += diff_x;
        this.bottom_left.y += diff_y;
        this.bottom_right.x -= diff_x;
        this.bottom_right.y += diff_y;

        this.sprite.scale.x -= (2 * diff_x) / this.orig_sprite_width;
        this.sprite.scale.y += (2 * diff_y) / this.orig_sprite_height;
      }

      this.last_mouse_x = pos.x;
      this.last_mouse_y = pos.y;
    }
  }

  end_resize(corner) {
    this[corner].resizing = false;

    const data = {
      width: this.sprite.width,
      height: this.sprite.height
    };

    if (this.history_index != null) {
      const history = this.history_set[this.history_index];
      if (history) {
        data.clear_history_id = history.id;
      }
    }

    req.patch(`sprites/${this.id}`, data)
      .then(sprite => {
        manager.update_histories();
        const index = manager.sprite_id_index;
        manager.sprite_id_history = manager.sprite_id_history.slice(0, index);
        manager.sprite_id_history.push(sprite.id);
        manager.sprite_id_index = manager.sprite_id_history.length;
      });
  }
};