
import { app } from '../../app.js';
import { theme } from '../../theme.js'
import PlugUserSprite from './plug-user-sprite.js';
import InfoUserSprite from './info-user-sprite.js';

class LabelManager {
  constructor() {
  }

  create_label(options) {
    this.plug = new PlugUserSprite(options);
    this.info = new InfoUserSprite(options);
    // create_wire();
  }

  create_plug(options) {
    this.plug = new PlugUserSprite(options);
  }

  create_info(options) {
    this.info = new InfoUserSprite(options);
  }

  create_wire() {
    const border_color = theme.get_darker_shade();
    this.line = new PIXI.Graphics();

    const x = this.plug.x + this.plug.width / 2;
    const y = this.plug.y - this.plug.height / 2;
    this.line.lineStyle(2, border_color, 1, 1);
    this.line.moveTo(x, y);
    this.line.lineTo(this.info.x, this.info.y + this.info.height);
  }
}

export const labelManager = new LabelManager;