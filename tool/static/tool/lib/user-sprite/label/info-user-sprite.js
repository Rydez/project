
import { app } from '../../app.js';
import { theme } from '../../theme.js'
import UserSprite from '../user-sprite.js';

const defaults = {
  info: {
    offset_x: 200,
    offset_y: -100,
    text: 'Example text here...',
    padding: 20
  }
};

export default class InfoUserSprite extends UserSprite {
  constructor(options) {
    super(options)
    const border_color = theme.get_darker_shade();
    const x = window.innerWidth / 2 + defaults.info.offset_x;
    const y = window.innerHeight / 2 + defaults.info.offset_y;
    const padding = defaults.info.padding;

    const text = new PIXI.Text(defaults.info.text, theme.text);
    text.anchor.set(0.5);
    const width = text.width + padding;
    const height = text.height + padding;

    this.container.x = this.options.x || x;
    this.container.y = this.options.y || y;

    text.x = 0;
    text.y = 0;

    const graphic = new PIXI.Graphics();
    graphic.lineStyle(2, border_color, 1, 1);
    graphic.beginFill(0x000000, 0.4);
    graphic.drawRect(0, 0, width, height);

    const texture = app.pixi_app.renderer.generateTexture(graphic)
    this.sprite = new PIXI.Sprite(texture);
    this.sprite.anchor.set(0.5);
    this.sprite.interactive = true;
    this.sprite.buttonMode = true;

    this.z = this.options.z == null ? app.pixi_app.stage.children.length : this.options.z;

    if (this.options.is_new) {
      this.save_sprite({
        x: this.container.x,
        y: this.container.y,
        z: this.z,
        page: app.options.page_id,
        type: 'info'
      });
    }
    else {
      this.id = this.options.id;
      this.container.name = this.id.toString();
      // this.history_set = this.options.history_set;
      // this.history_index = this.history_set.length;
    }

    this.sprite.addChild(text);
    this.container.addChild(this.sprite);

    this.initialize();

    app.pixi_app.stage.addChild(this.container);
  }
}