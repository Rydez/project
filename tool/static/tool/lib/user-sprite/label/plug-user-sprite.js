
import { app } from '../../app.js';
import { theme } from '../../theme.js'
import UserSprite from '../user-sprite.js';

const defaults = {
  plug: {
    radius: 10
  }
};

export default class PlugUserSprite extends UserSprite {
  constructor(options) {
    super(options)
    const border_color = theme.get_darker_shade();
    const center_x = window.innerWidth / 2;
    const center_y = window.innerHeight / 2;

    this.container.x = this.options.x || center_x;
    this.container.y = this.options.y || center_y;

    this.radius = this.options.radius || defaults.plug.radius;
    const graphic = new PIXI.Graphics();
    graphic.lineStyle(2, border_color, 1, 1);
    graphic.beginFill(0x000000, 0.4);
    graphic.drawCircle(0, 0, this.raduis);

    const texture = app.pixi_app.renderer.generateTexture(graphic)
    this.sprite = new PIXI.Sprite(texture);
    this.sprite.interactive = true;
    this.sprite.buttonMode = true;
    this.sprite.anchor.set(0.5);

    this.z = this.options.z == null ? app.pixi_app.stage.children.length : this.options.z;

    if (this.options.is_new) {
      this.save_sprite({
        x: this.container.x,
        y: this.container.y,
        z: this.z,
        radius: this.radius,
        page: app.options.page_id,
        type: 'plug'
      });
    }
    else {
      this.id = this.options.id;
      this.container.name = this.id.toString();
      // this.history_set = this.options.history_set;
      // this.history_index = this.history_set.length;
    }

    this.container.addChild(this.sprite);

    this.initialize();

    app.pixi_app.stage.addChild(this.container);
  }
}