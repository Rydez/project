import { app } from '../app.js';
import { req } from '../req.js';
import { resize } from '../resize.js';
import ImageUserSprite from './image-user-sprite.js';
import { labelManager } from './label/label-manager.js';

class UserSpriteManager {
  constructor() {
  }

  initialize() {
    this.user_sprites = [];
    this.load_existing_sprites();
  }

  rearrange(action) {
    const selected_child = this.get_selected_child();
    const children = app.pixi_app.stage.children;
    if (!selected_child || !children || children.length === 0) {
      return;
    }

    this[action].call(this, selected_child, children);
  }

  foreground(selected_child, children) {
    children.splice(selected_child.index, 1);
    children.push(selected_child.child);
    this.save_z_indices(selected_child.child.name);
  }

  forward(selected_child, children) {
    if (selected_child.index === children.length - 1) {
      return;
    }

    children.splice(selected_child.index, 1);
    children.splice(selected_child.index + 1, 0, selected_child.child);
    this.save_z_indices(selected_child.child.name);
  }

  backward(selected_child, children) {
    if (selected_child.index === 0) {
      return;
    }

    const new_index = selected_child.index - 1;
    children.splice(selected_child.index, 1);
    children.splice(new_index, 0, selected_child.child);
    this.save_z_indices(selected_child.child.name);
  }

  background(selected_child, children) {
    children.splice(selected_child.index, 1);
    children.unshift(selected_child.child);
    this.save_z_indices(selected_child.child.name);
  }

  get_selected_child() {
    for (let user_sprite of this.user_sprites) {
      if (user_sprite.selected) {
        const child_name = user_sprite.container.name;
        const children = app.pixi_app.stage.children;
        for (let i = 0; i < children.length; i++) {
          if (children[i].name === child_name) {
            return {
              index: i,
              child: children[i],
              sprite: user_sprite.sprite
            };
          }
        }
      }
    }
  }

  save_z_indices(selected_child_name) {
    const children = app.pixi_app.stage.children;
    for (let i = 0; i < children.length; i++) {
      for (let user_sprite of this.user_sprites) {
        if (user_sprite.container.name !== children[i].name) {
          continue;
        }

        const data = { z: i };
        user_sprite.z = data.z;

        data.skip_history_creation = true;
        if (children[i].name === selected_child_name) {
          data.skip_history_creation = false;
          if (user_sprite.history_index != null) {
            const history = user_sprite.history_set[user_sprite.history_index];
            if (history) {
              data.clear_history_id = history.id;
            }
          }
        }

        if (data.z === 0) {
          user_sprite.sprite.interactive = false;
          user_sprite.sprite.buttonMode = false;
          const center_x = window.innerWidth / 2;
          const center_y = window.innerHeight / 2;
          user_sprite.container.x = center_x;
          user_sprite.container.y = center_y;
          user_sprite.unselect();
          data.x = user_sprite.container.x;
          data.y = user_sprite.container.y;
        }
        else {
          user_sprite.sprite.interactive = true;
          user_sprite.sprite.buttonMode = true;
        }

        req.patch(`sprites/${user_sprite.id}`, data)
          .then(sprite => {
            if (!data.skip_history_creation) {
              this.update_histories();

              const index = this.sprite_id_index;
              this.sprite_id_history = this.sprite_id_history.slice(0, index);
              this.sprite_id_history.push(sprite.id);
              this.sprite_id_index = this.sprite_id_history.length;
            }
          });
      }
    }
  }

  load_existing_sprites() {
    req.get(`pages/${app.options.page_id}`)
      .then(page => {
        this.sprite_id_history = [];
        this.sprite_set = page.sprite_set;
        this.sprite_set.sort((s1, s2) => s1.z - s2.z);

        if (app.options.mode !== 'edit') {
          const [background] = this.sprite_set;
          resize.set_background(background);
        }

        for (let sprite of this.sprite_set) {
          if (sprite.type === 'image') {
            const url_parts = sprite.image.split('media');
            new ImageUserSprite(`../../../../static/${url_parts[1]}`, sprite);
          }
          else if (sprite.type === 'plug') {
            labelManager.create_plug(sprite);
          }
          else if (sprite.type === 'info') {
            labelManager.create_info(sprite);
          }
        }

        let added_count = 0;
        const arrange_sprites = () => {
          added_count += 1;
          if (added_count === this.user_sprites.length) {
            this.sort_stage_children()
            if (app.options.mode !== 'edit') {
              this.transform_editor_to_display();
            }
            else {
              this.transform_to_center();
            }

            window.dispatchEvent(new Event('load_user_sprites'));
          }
        };

        for (let user_sprite of this.user_sprites) {
          if (user_sprite.container.parent) {
            arrange_sprites();
          }
          else {
            user_sprite.container.once('added', () => {
              arrange_sprites();
            });
          }
        }
      });
  }

  transform_editor_to_display() {
    const [background] = this.user_sprites;
    const center_x = background.sprite.width / 2;
    const center_y = background.sprite.height / 2;
    const offset_x = background.container.x - center_x;
    const offset_y = background.container.y - center_y;
    for (let user_sprite of this.user_sprites) {
      user_sprite.container.x -= offset_x;
      user_sprite.container.y -= offset_y;
    }
  }

  transform_to_center() {
    const [background] = app.pixi_app.stage.children;
    if (!background) {
      return;
    }

    const center_x = window.innerWidth / 2;
    const center_y = window.innerHeight / 2;
    const offset_x = center_x - background.x;
    const offset_y = center_y - background.y;
    background.x += offset_x;
    background.y += offset_y;

    // Clear all histories from last session
    const data = {
      clear_history: true,
      skip_history_creation: true,
      x: background.x,
      y: background.y
    };

    req.patch(`sprites/${background.name}`, data)

    const children = app.pixi_app.stage.children;
    for (let i = 1; i < children.length; i++) {
      children[i].x += offset_x;
      children[i].y += offset_y;

      const data = {
        skip_history_creation: true,
        x: children[i].x,
        y: children[i].y
      };

      req.patch(`sprites/${children[i].name}`, data);
    }
  }

  sort_stage_children() {
    app.pixi_app.stage.children.sort((c1, c2) => {
      let s1;
      let s2;
      for (let user_sprite of this.user_sprites) {
        if (user_sprite.container.name === c1.name) {
          s1 = user_sprite;
        }
        else if (user_sprite.container.name === c2.name) {
          s2 = user_sprite;
        }
      }

      return s1.z - s2.z;
    });
  }

  sprite_from_file(e) {
    const reader = new FileReader();
    reader.addEventListener('load' , () => {
      new ImageUserSprite(reader.result, {is_new: true});
    });

    const file = e.dataTransfer.files[0];
    if (file) {
      reader.readAsDataURL(file);
    }
  }

  create_label() {
    labelManager.create_label({is_new: true});
  }

  undo() {
    if (this.sprite_id_index == null) {
      this.sprite_id_index = this.sprite_id_history.length;
    }

    if (this.sprite_id_index === 0) {
      return;
    }

    this.sprite_id_index--;
    const sprite_id = this.sprite_id_history[this.sprite_id_index];
    for (let user_sprite of this.user_sprites) {
      if (user_sprite.id === sprite_id) {
        user_sprite.history_index--;
        const history_index = user_sprite.history_index;
        const history_state = user_sprite.history_set[history_index];
        const data = Object.assign({}, history_state);
        if (history_index < user_sprite.history_set.length - 1) {
          data.skip_history_creation = true;
        }

        this.change_historical_state(data);
        return;
      }
    }
  }

  redo() {
    if (this.sprite_id_index == null ||
        this.sprite_id_index > this.sprite_id_history.length - 1)
    {
      return;
    }

    const sprite_id = this.sprite_id_history[this.sprite_id_index];
    this.sprite_id_index++;
    for (let user_sprite of this.user_sprites) {
      if (user_sprite.id === sprite_id) {
        user_sprite.history_index++;
        const history_index = user_sprite.history_index;
        const history_state = user_sprite.history_set[history_index];
        const data = Object.assign({}, history_state);
        data.skip_history_creation = true;
        this.change_historical_state(data);
        return;
      }
    }
  }

  change_historical_state(data) {
    const sprite_id = data.sprite;

    delete data.id;
    delete data.page;
    delete data.sprite;

    for (let prop in data) {
      if (data[prop] === null) {
        delete data[prop];
      }
    }

    req.patch(`sprites/${sprite_id}`, data)
      .then(sprite => {
        for (let user_sprite of this.user_sprites) {
          if (sprite.id === user_sprite.id) {
            for (let prop in sprite) {
              if (!data[prop]) {
                continue;
              }

              // Container props
              if (['x', 'y'].includes(prop)) {
                user_sprite.container[prop] = sprite[prop];
              }

              // Sprite props
              if (['width', 'height'].includes(prop)) {
                user_sprite.sprite[prop] = sprite[prop];
                user_sprite.redraw_selection();
              }

              // User sprite props
              if ('z' === prop) {
                const children = app.pixi_app.stage.children;
                for (let i = 0; i < children.length; i++) {
                  if (children[i].name === user_sprite.container.name) {
                    const child = children[i];
                    children.splice(i, 1);
                    children.splice(sprite.z, 0, child);
                  }
                }

                this.save_z_indices();
              }
            }

            user_sprite.history_set = sprite.history_set;
          }
        }
      });
  }

  update_histories() {
    for (let user_sprite of this.user_sprites) {
      req.get(`sprites/${user_sprite.id}`)
        .then(sprite => {
          user_sprite.history_set = sprite.history_set;
          user_sprite.history_index = sprite.history_set.length;
        });
    }
  }
}

window.lib = window.lib || {};
window.lib.editor = window.lib.editor || {};
window.lib.editor.userSpriteManager = window.lib.editor.userSpriteManager || new UserSpriteManager();
export const userSpriteManager = window.lib.editor.userSpriteManager;