from django.db import models


class Page(models.Model):
    slug = models.SlugField(max_length=100, default='unnamed')
    background_color = models.PositiveIntegerField(null=True)

    def __str__(self):
        return self.slug


class Sprite(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='sprites', null=True)
    x = models.FloatField()
    y = models.FloatField()
    z = models.PositiveIntegerField()
    width = models.FloatField(null=True)
    height = models.FloatField(null=True)
    radius = models.FloatField(null=True)
    type = models.TextField()


class History(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    sprite = models.ForeignKey(Sprite, on_delete=models.CASCADE)
    x = models.FloatField(null=True)
    y = models.FloatField(null=True)
    z = models.PositiveIntegerField(null=True)
    width = models.FloatField(null=True)
    height = models.FloatField(null=True)