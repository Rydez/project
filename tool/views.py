from django.views.generic import TemplateView
from .models import Page, Sprite, History
from .serializers import PageSerializer, SpriteSerializer, HistorySerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import APIException


class IndexView(TemplateView):
  template_name = 'tool/main/main.pug'


class PageView(ModelViewSet):
  permission_classes = (AllowAny,)
  queryset = Page.objects.all()
  serializer_class = PageSerializer


class SpriteView(ModelViewSet):
  permission_classes = (AllowAny,)
  queryset = Sprite.objects.all()
  serializer_class = SpriteSerializer

  # def perform_update(self, serializer):
  #   serializer.save()


class HistoryView(ModelViewSet):
  permission_classes = (AllowAny,)
  queryset = History.objects.all()
  serializer_class = HistorySerializer
