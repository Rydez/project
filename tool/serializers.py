from django.db import transaction
from rest_framework import serializers
from rest_framework.utils import model_meta

from .models import Page, Sprite, History
from drf_extra_fields.fields import Base64ImageField


class HistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        fields = '__all__'


class SpriteSerializer(serializers.ModelSerializer):
    skip_history_creation = serializers.BooleanField(write_only=True, required=False)
    clear_history = serializers.BooleanField(write_only=True, required=False)
    clear_history_id = serializers.IntegerField(write_only=True, required=False)

    image = Base64ImageField(required=False)

    history_set = serializers.SerializerMethodField()

    class Meta:
        model = Sprite
        fields = '__all__'

    def get_history_set(self, obj):
        histories = History.objects.filter(sprite_id=obj.id).order_by('id')
        return HistorySerializer(histories, many=True, read_only=True).data

    @transaction.atomic
    def update(self, instance, validated_data):

        # Clear history from id on
        clear_history_id = validated_data.pop('clear_history_id', False)
        if clear_history_id:
            History.objects.filter(
                # Maybe all later historyies should be removed
                # sprite_id=instance.id,
                id__gte=clear_history_id
            ).delete()

        # Check if we should clear all page history
        clear_history = validated_data.pop('clear_history', False)
        if clear_history:
            History.objects.filter(page_id=instance.page_id).delete()

        # Check if we should create a history record
        skip_history_creation = validated_data.pop('skip_history_creation', False)
        if not skip_history_creation:
            History.objects.create(
                page_id=instance.page_id,
                sprite_id=instance.id,
                x=instance.x,
                y=instance.y,
                z=instance.z,
                width=instance.width,
                height=instance.height
            )

        # Call original method
        return super(SpriteSerializer, self).update(instance, validated_data)


class PageSerializer(serializers.ModelSerializer):
    sprite_set = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = '__all__'

    def get_sprite_set(self, obj):
        sprites = Sprite.objects.filter(page_id=obj.id).order_by('id')
        return SpriteSerializer(sprites, many=True, read_only=True).data
