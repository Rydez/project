# Generated by Django 2.1.1 on 2018-09-23 12:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tool', '0003_auto_20180916_1836'),
    ]

    operations = [
        migrations.AddField(
            model_name='sprite',
            name='page',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='tool.Page'),
            preserve_default=False,
        ),
    ]
