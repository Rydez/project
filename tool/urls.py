from django.urls import path, include, re_path
from .views import IndexView, PageView, SpriteView, HistoryView
# from tool.views import StartMenuView, EditorView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('pages', PageView)
router.register('sprites', SpriteView)
router.register('histories', HistoryView)

app_name = 'tool'
urlpatterns = [
  path('api/', include(router.urls)),
  re_path(r'^', IndexView.as_view(), name='index'),
]