from django.contrib import admin

from .models import Page, Sprite

admin.site.register(Page)
admin.site.register(Sprite)