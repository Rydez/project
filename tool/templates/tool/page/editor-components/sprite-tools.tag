sprite-tools
  .tools.left(ref="left_tools")
    .slide-panel.left(class="{ left_hovering ? 'hovering' : '' }")
      .top
        .undo-redo
          button.undo(onclick="{ undo }") <=
          button.redo(onclick="{ redo }") =>
        .sprite-card(each="{ user_sprites }")
          img(src="{ sprite.texture.baseTexture.imageUrl }")
      .bottom
        button.single(onclick="{ new_label }") Add Label

  .tools.right(ref="right_tools")
    .slide-panel.right(class="{ right_hovering ? 'hovering' : '' }")
      .top
        button.single(onclick="{ foreground }") Foreground
        button.single(onclick="{ forward }") Bring forward
        button.single(onclick="{ backward }") Move backward
        button.single(onclick="{ background }") Background
      .bottom
        button.single(onclick="{ open_settings_modal }") Settings

  modal(ref="settings_modal")
    .title Settings
    .content
      .red.bold(if="{ parent.error }")
        | Please enter valid hex color code
      label Background Color:
        input(
          ref="background_input"
          type="text"
          oninput="{ parent.change_background }"
        )
      .buttons
        button.cancel Ok


  script.
    this.user_sprites = [];

    this.on('mount', () => {
      this.hovering = false;

      this.refs.left_tools.onmouseover = () => {
        this.update({ left_hovering: true });
      };

      this.refs.left_tools.onmouseout = () => {
        this.update({ left_hovering: false });
      };

      this.refs.right_tools.onmouseover = () => {
        this.update({ right_hovering: true });
      };

      this.refs.right_tools.onmouseout = () => {
        this.update({ right_hovering: false });
      };

      const get_user_sprites = () => {
        this.user_sprites = lib.editor.userSpriteManager.user_sprites;
        this.update();
        window.removeEventListener('load_user_sprites', get_user_sprites);
      };

      if (
        window.lib &&
        window.lib.editor &&
        window.lib.editor.userSpriteManager &&
        window.lib.editor.userSpriteManager.user_sprites
      ) {
        this.user_sprites = lib.editor.userSpriteManager.user_sprites;
        this.update();
      }
      else {
        window.addEventListener('load_user_sprites', get_user_sprites);
      }
    });

    this.foreground = () => {
      lib.editor.userSpriteManager.rearrange('foreground');
    };

    this.forward = () => {
      lib.editor.userSpriteManager.rearrange('forward');
    };

    this.backward = () => {
      lib.editor.userSpriteManager.rearrange('backward');
    };

    this.background = () => {
      lib.editor.userSpriteManager.rearrange('background');
    };

    this.undo = () => {
      lib.editor.userSpriteManager.undo();
    };

    this.redo = () => {
      lib.editor.userSpriteManager.redo();
    };

    this.new_label = () => {
      lib.editor.userSpriteManager.create_label();
    };

    this.open_settings_modal = () => {
      const hex = lib.editor.theme.background.toString(16);
      this.refs.settings_modal.refs.background_input.value = '#' + hex;
      this.refs.settings_modal.show();
    };

    this.change_background = (ev) => {
      this.error = false;
      const value = ev.target.value;

      if (value === '') {
        this.refs.settings_modal.refs.background_input.value = '#';
      }

      if (value.match(/^#([0-9a-f]{3}|[0-9a-f]{6})$/i)) {
        lib.editor.theme.new_background_color(value);
      }
      else {
        this.error = true;
      }
    };
