page-display

  script.
    this.on('mount', () => {
      this.editor_options = {
        page_id: this.opts.id,
      };

      if (window.lib && window.lib.editor && window.lib.editor.app) {
        lib.editor.app.initialize(this.editor_options);
      }
      else {
        window.addEventListener('load_pixi_app', this.initialize_editor);
      }
    });

    this.on('before-unmount', () => {
      if (window.lib && window.lib.editor && window.lib.editor.app) {
        lib.editor.app.destroy();
      }
    });

    this.initialize_editor = () => {
      lib.editor.app.initialize(this.editor_options);
      window.removeEventListener('load_pixi_app', this.initialize_editor);
    };