app
  navigation(if="{ current_route && !current_route.no_nav }")
  main

  script.
    const routes = {
      '': {path: 'home', title: 'Home'},
      'pages': {path: 'page-list', title: 'Pages'},
      'pages/*/edit': {path: 'page-edit', title: 'Editor', no_nav: true},
      'pages/*/display': {path: 'page-display', title: 'Display', no_nav: true},
    };

    const [title] = document.getElementsByTagName('title');
    route((collection, id, action) => {
      let key = collection;
      key += id ? '/*' : '';
      key += action ? `/${action}` : '';

      this.update({ current_route: routes[key] });

      id = Number(id);
      riot.mount('main', this.current_route.path, { id });
      title.innerHTML = this.current_route.title;
    });